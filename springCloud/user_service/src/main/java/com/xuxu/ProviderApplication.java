package com.xuxu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.xuxu.mapper")
//@EnableEurekaClient //这个注解表示是 eureka的客户端，需要将自己注册到eureka服务注册中心上，这个只能用在Eureka注册中心上面
@EnableDiscoveryClient //这个注解和上面的注解功能一样，用哪个都可以，这个可以适用于其他类似的注册中心
public class ProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }
}
