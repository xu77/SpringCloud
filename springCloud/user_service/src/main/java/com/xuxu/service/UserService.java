package com.xuxu.service;

import com.xuxu.mapper.UserMapper;
import com.xuxu.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    public User findById(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }
}
