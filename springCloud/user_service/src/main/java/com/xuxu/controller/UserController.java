package com.xuxu.controller;

import com.xuxu.pojo.User;
import com.xuxu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户控制层
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 根据id查询用户
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public User findById(@PathVariable Long id){

        return userService.findById(id);
    }
}
