package com.xuxu.mapper;

import com.xuxu.pojo.User;
import tk.mybatis.mapper.common.Mapper;

//通用mapper
public interface UserMapper extends Mapper<User> {

}
